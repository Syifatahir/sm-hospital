<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekamMedisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::drop('rekam_medis');
        Schema::create('rekam_medis', function (Blueprint $table) {
            $table->increments('no_pendaftaran');
            $table->enum('jenis_pendaftaran', ['BPJS', 'Umum']);
            $table->string('surat_rujukan', 100);
            $table->dateTime('tgl_pendaftaran');

            $table->integer('no_rm')->unsigned();  
            $table->foreign('no_rm')->references('no_rm')->on('pasien')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('id_dokter')->unsigned();  
            $table->foreign('id_dokter')->references('id_dokter')->on('dokter')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekam_medis');
    }
}
