<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasien', function (Blueprint $table) {
            $table->increments('no_rm', 11);
            $table->string('email', 50)->unique();
            $table->string('nama', 100);
            $table->integer('NIK');
            $table->string('alamat', 50);
            $table->string('tempat_lahir', 20);
            $table->date('tgl_lahir');
            $table->string('umur', 10);
            $table->string('jk', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien');
    }
}
