@extends('layouts.app')
@section('content')

<?php  ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9"> 
            <div class="card">
                <div class="card-header">{{ __('Form Pendaftaran Rekam Medis') }}</div>

                <div class="card-body">

                    @if(session()->has('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif

                    @if(session()->has('errors'))
                        <?php $errors = session()->get('errors'); ?>
                        <div class="alert alert-danger">
                            <?php echo implode(',', $errors->get('surat_rujukan')); ?> <br>
                            <?php echo implode(',', $errors->get('tgl_pendaftaran')); ?>
                        </div>
                    @endif
                    <form method="POST" action="{{ route('rekam_medis-store') }}">
                    <input id="no_rm" type="hidden" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="no_rm" value="<?php echo $pasien->id; ?>">
                    <input id="id_dokter" type="hidden" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="id_dokter" value="<?php echo $dokter->id_dokter; ?>">
                        @csrf
                        <br>

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Dokter') }}</label>

                            <div class="col-md-6">
                            <input id="" type="text" class="form-control" name="" value="<?php echo $dokter->nama_dokter; ?>" disabled> 
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jenis_pendaftaran" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Pendaftaran') }}</label>

                            <div class="col-md-6">
                            <select name="jenis_pendaftaran" id="jenis_pendaftaran" class="form-control{{ $errors->has('Jenis_Pendaftaran') ? ' is-invalid' : '' }}">
                                <option disabled>Pilih Jenis Pendaftaran</option>
                                <option value="BPJS">BPJS</option>
                                <option value="Umum">Umum</option>
                            </select>

                                @if ($errors->has('jenis_pendaftaran'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surat_rujukan" class="col-md-4 col-form-label text-md-right">{{ __('No Surat Rujukan') }}</label>

                            <div class="col-md-6">
                            <input id="surat_rujukan" type="number" class="form-control" name="surat_rujukan" value="" autofocus placeholder="Wajib diisi oleh pasien BPJS">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tgl_pendaftaran" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Pendaftaran') }}</label>

                            <div class="col-md-6">
                            <input id="tgl_pendaftaran" type="date" class="form-control{{ $errors->has('tgl_pendaftaran') ? ' is-invalid' : '' }}" name="tgl_pendaftaran" value="" autofocus>

                                @if ($errors->has('tgl_pendaftaran'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection