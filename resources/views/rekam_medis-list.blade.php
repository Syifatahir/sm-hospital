@extends('layouts.app')

@section('content')
<?php if ($rekam_medis->isEmpty() != TRUE) { ?>
<?php foreach ($rekam_medis as $rowRekamMedis){ ?>
<div class="modal fade" id="exampleModal<?php echo $rowRekamMedis->no_pendaftaran; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi penghapusan data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah anda yakin akan membatalkan perjanjian dengan no pendaftaran: <?php echo $rowRekamMedis->no_pendaftaran; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <a href="{{route('rekam_medis-destroy', ['id' => $rowRekamMedis->no_pendaftaran])}}" class="btn btn-sm btn-danger mr-2 mb-2">
          Batalkan Perjanjian
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>
    
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissable fade show">
                    {{ session()->get('success') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3 class="text-center">Daftar Perjanjian anda</h3>
                <?php if($rekam_medis->isEmpty() != TRUE) { ?>
                    <?php foreach ($rekam_medis as $rowRekamMedis) { ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <h5><?php echo $rowRekamMedis->name;?></h5>
                                </div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-6">
                                            <h5>No Pendaftaran: <?php echo $rowRekamMedis->no_pendaftaran;?></h5>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal<?php echo $rowRekamMedis->no_pendaftaran; ?>">
                                                Batalkan Perjanjian
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <p class="card-text">Dokter : <?php echo $rowRekamMedis->nama_dokter; ?></p>
                            <p class="card-text">Jenis Pembayaran : <?php echo $rowRekamMedis->jenis_pendaftaran; ?></p>
                            <p class="card-text">Spesialis : <?php echo $rowRekamMedis->spesialis;?></p>
                            <p class="card-text">Jadwal : <?php echo date("j F Y", strtotime($rowRekamMedis->tgl_pendaftaran));?></p>
                        </div>
                    </div>
                    <?php } ?>
                <?php } else { ?>
                    <h1>Anda Belum memiliki riwayat perjanjian.</h1>
                <?php } ?>    
            </div>
        </div>
    </div>
    
@endsection