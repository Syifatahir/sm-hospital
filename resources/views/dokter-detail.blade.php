@extends('layouts.dashboard')
@section('content')

<?php ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9"> 
            <div class="card">
                <div class="card-header">{{ __('Update data Dokter') }}</div>

                <div class="card-body">

                    @if(session()->has('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif

                    <form method="POST" action="{{ route('dokter-edit-detail') }}">
                        <input id="id_dokter" type="hidden" class="form-control{{ $errors->has('nama_dokter') ? ' is-invalid' : '' }}" name="id_dokter" value="<?php echo $dokter->id_dokter; ?>">
                        @csrf
                        <br>

                        <div class="form-group row">
                            <label for="nama_dokter" class="col-md-4 col-form-label text-md-right">{{ __('Nama Dokter') }}</label>

                            <div class="col-md-6">
                                <input id="nama_dokter" type="text" class="form-control{{ $errors->has('nama_dokter') ? ' is-invalid' : '' }}" name="nama_dokter" value="<?php echo $dokter->nama_dokter; ?>" autofocus>

                                @if ($errors->has('nama_dokter'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="spesialis" class="col-md-4 col-form-label text-md-right">{{ __('Spesialis') }}</label>

                            <div class="col-md-6">
                                <input id="spesialis" type="text" class="form-control{{ $errors->has('spesialis') ? ' is-invalid' : '' }}" name="spesialis" value="<?php echo $dokter->spesialis; ?>" autofocus>

                                @if ($errors->has('spesialis'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                 
                      
                        <div class="form-group row">
                            <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('No Handphone') }}</label>

                            <div class="col-md-4">
                                <input id="no_hp" type="text" class="form-control{{ $errors->has('no_hp') ? ' is-invalid' : '' }}" name="no_hp" value="<?php echo $dokter->no_hp; ?>" autofocus>

                                @if ($errors->has('no_hp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>
                            <div class="col-md-6">
                                <textarea id="alamat" type="text" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" name="alamat" value="" rows="3"><?php echo $dokter->alamat; ?></textarea>

                                @if ($errors->has('alamat'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('alamat') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection