@extends('layouts.dashboard')
@section('content')

<!-- table kategori -->
<div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Rekam Medis</h4>
          </div>
        </div>
        <div class="card-body">
          <form action="{{URL('/admin/rekam_medis')}}">
            <div class="row">
              <div class="col">
                <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Cari Rekam Medis Pasien...">
              </div>
              <div class="col-auto">
                <button class="btn btn-primary">
                  Cari
                </button>
              </div>
            </div>
          </form>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable fade show">
              {{ session()->get('success') }}
            </div>
        @endif
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr align="center">
                  <th width="50px">No</th>
                  <th>Nama Pasien</th>
                  <th>No Pendaftaran</th>
                  <th>Nama Dokter</th>
                  <th>Spesialis</th>
                  <th>Jenis Pendaftaran</th>
                  <th>Jadwal</th>
                  <th>No RM</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($rekam_medis->isEmpty() != TRUE) { ?>
                  <?php $no=1; foreach($rekam_medis as $rowRekamMedis) { ?>
                  <tr align="center"> 
                    <td><?php echo $no; ?></td>
                    <td><?php echo $rowRekamMedis->nama; ?></td>
                    <td><?php echo $rowRekamMedis->no_pendaftaran; ?></td>
                    <td><?php echo $rowRekamMedis->nama_dokter; ?></td>
                    <td><?php echo $rowRekamMedis->spesialis; ?></td>
                    <td><?php echo $rowRekamMedis->jenis_pendaftaran; ?></td>
                    <td><?php echo date("j F Y", strtotime($rowRekamMedis->tgl_pendaftaran)); ?></td>
                    <td><?php echo $rowRekamMedis->no_rm; ?></td>
                  </tr>
                  <?php $no++; } ?>
                <?php } else { ?>
                  <tr>
                    <td colspan="11">
                      Data kosong!
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection