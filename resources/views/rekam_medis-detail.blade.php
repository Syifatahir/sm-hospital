@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="row">
            <div class="col-4">
                &nbsp;
            </div>
            <div class="col-4">
                <h5 class="text-center">Pendaftaran berhasil dibuat!</h5>
                <div class="card">
                    <div class="card-body">
                        <center><h2 class="">Detail Perjanjian</h2></center>
                        <hr>
                        <p class="card-text">Dokter: <?php echo $rekam_medis->nama_dokter; ?> </p>
                        <p class="card-text">Nama: <?php echo $rekam_medis->name;?></p>
                        <p class="card-text">Spesialis: <?php echo $rekam_medis->spesialis;?></p>
                        <p class="card-text">Jadwal: <?php echo date("j F Y", strtotime($rekam_medis->tgl_pendaftaran));?></p>
                    </div>
                </div>
                <p class="text-center">
                    Agar proses registrasi anda dapat diproses, silakan hubungi staff kami!
                </p>
                <a href="{{route('beranda')}}" class=" w-100     btn btn-primary">OK</a>
            </div>
        </div>
    </div>
    
@endsection