@extends('layouts.dashboard')
@section('content')
<!-- <?php  ?> -->
<?php if ($pasien->isEmpty() != TRUE) { ?>
<?php foreach ($pasien as $rowPasien){ ?>
<div class="modal fade" id="exampleModal<?php echo $rowPasien->no_rm; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi penghapusan data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah anda yakin akan menghapus data pasien atas nama: <?php echo $rowPasien->nama; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <a href="{{route('pasien-destroy', ['id' => $rowPasien->no_rm])}}" class="btn btn-sm btn-danger mr-2 mb-2">
          Hapus
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>
  <!-- table kategori -->
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Pasien</h4>
        </div>
        <div class="card-body">
          <form action ="{{URL('/admin/pasien')}}">
            <div class="row">
              <div class="col">
                <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Cari Pasien...">
              </div>
              <div class="col-auto">
                <button class="btn btn-primary" >
                  Cari
                </button>
              </div>
            </div>
          </form>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable fade show">
              {{ session()->get('success') }}
            </div>
        @endif
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Nama Lengkap</th>
                  <th>No RM</th>
                  <th>E-Mail Address</th>
                  <th>NIK</th>
                  <th>Alamat</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>Umur</th>
                  <th>Jenis Kelamin</th>
                  <th colspan="2"><center>Aksi</center></th>
                </tr>
              </thead>
              <tbody>
                <?php if ($pasien->isEmpty() != TRUE) { ?>
                  <?php $no=1; foreach($pasien as $rowPasien) { ?>
                  <tr> 
                    <td><?php echo $no; ?></td>
                    <td><?php echo $rowPasien->nama; ?></td>
                    <td><?php echo $rowPasien->no_rm; ?></td>
                    <td><?php echo $rowPasien->email; ?></td>
                    <td><?php echo $rowPasien->NIK; ?></td>
                    <td><?php echo $rowPasien->alamat; ?></td>
                    <td><?php echo $rowPasien->tempat_lahir; ?></td>
                    <td><?php echo $rowPasien->tgl_lahir; ?></td>
                    <td><?php echo $rowPasien->umur; ?></td>
                    <td><?php echo $rowPasien->jk; ?></td>
                    <td>
                      <a href="{{route('pasien-detail', ['id' => $rowPasien->no_rm])}}" class="btn btn-sm btn-primary mr-2 mb-2">
                        Edit
                      </a>
                    </td>
                    <td>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal<?php echo $rowPasien->no_rm; ?>">
                        Hapus
                      </button>
                    </td>
                  </tr>
                  <?php $no++; } ?>
                <?php } else { ?>
                  <tr>
                    <td colspan="11">
                      Data kosong!
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection