@extends('layouts.dashboard')
@section('content')

<?php  ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9"> 
            <div class="card">
                <div class="card-header">{{ __('Form Jadwal') }}</div>

                <div class="card-body">

                    @if(session()->has('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif

                    <form method="POST" action="{{ route('jadwal-edit-detail') }}">
                    <input id="id_jadwal" type="hidden" class="form-control{{ $errors->has('id_jadwal') ? ' is-invalid' : '' }}" name="id_jadwal" value="<?php echo $jadwal->id_jadwal; ?>">
                        @csrf
                        <br>

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Dokter') }}</label>

                            <div class="col-md-6">
                            <select name="id_dokter" id="nama_dokter" class="form-control{{ $errors->has('nama_dokter') ? ' is-invalid' : '' }}">
                                <option disabled>Pilih Dokter</option>
                                <option selected value="<?php echo $jadwal->id_dokter; ?>"><?php echo $jadwal->nama_dokter; ?></option>
                                <?php
                                    if ($dokter->isEmpty() != TRUE) { ?>
                                        <?php foreach ($dokter as $rowDokter) { ?>
                                            <?php if ($rowDokter->id_dokter != $jadwal->id_dokter) { ?>
                                            <option value="<?php echo $rowDokter->id_dokter;?>">
                                                <?php echo $rowDokter->nama_dokter; ?>
                                            </option>
                                            <?php } ?>
                                        <?php } 
                                    } else { ?>
                                        <option disabled>Data kosong!</option>
                                    <?php }
                                ?>
                            </select>

                                @if ($errors->has('nama_dokter'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_poli" class="col-md-4 col-form-label text-md-right">{{ __('Nama Poli') }}</label>

                            <div class="col-md-6">
                            <select name="id_poli" id="nama_poli" class="form-control{{ $errors->has('nama_poli') ? ' is-invalid' : '' }}">
                                <option disabled>Pilih Poli</option>
                                <option selected value="<?php echo $jadwal->id_poli; ?>"><?php echo $jadwal->nama_poli; ?></option>
                                <?php
                                    if ($poli->isEmpty() != TRUE) { ?>
                                        <?php foreach ($poli as $rowpoli) { ?>
                                            <?php if ($rowpoli->id_poli != $jadwal->id_poli) { ?>
                                            <option value="<?php echo $rowpoli->id_poli;?>">
                                                <?php echo $rowpoli->nama_poli; ?>
                                            </option>
                                            <?php } ?>
                                        <?php } 
                                    } else { ?>
                                        <option disabled>Data kosong!</option>
                                    <?php }
                                ?>
                            </select>

                                @if ($errors->has('nama_poli'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hari" class="col-md-4 col-form-label text-md-right">{{ __('Hari') }}</label>

                            <div class="col-md-4">
                                <input id="hari" type="text" class="form-control{{ $errors->has('hari') ? ' is-invalid' : '' }}" name="hari" value="<?php echo $jadwal->hari; ?>" autofocus>

                                @if ($errors->has('hari'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
           
                        <div class="form-group row">
                            <label for="jam" class="col-md-4 col-form-label text-md-right">{{ __('Jam') }}</label>

                            <div class="col-md-4">
                                <input id="jam" type="text" class="form-control{{ $errors->has('jam') ? ' is-invalid' : '' }}" name="jam" value="<?php echo $jadwal->jam; ?>" autofocus>

                                @if ($errors->has('jam'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection