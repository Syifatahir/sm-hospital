@extends('layouts.dashboard')
@section('content')
<!-- <?php  ?> -->
<?php if ($dokter->isEmpty() != TRUE) { ?>
<?php foreach ($dokter as $rowDokter){ ?>
<div class="modal fade" id="exampleModal<?php echo $rowDokter->id_dokter; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi penghapusan data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah anda yakin akan menghapus data pasien atas nama: <?php echo $rowDokter->nama_dokter; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <a href="{{route('dokter-destroy', ['id' => $rowDokter->id_dokter])}}" class="btn btn-sm btn-danger mr-2 mb-2">
          Hapus
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>
  <!-- table pendaftaran -->
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Data Dokter</h4>
        </div>
        <div class="card-body">
          <form action="{{URL('/admin/dokter')}}">
            <div class="row">
              <div class="col">
                <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Cari Dokter...">
              </div>
              <div class="col-auto">
                <button class="btn btn-primary">
                  Cari
                </button>
              </div>
            </div>
          </form>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable fade show">
              {{ session()->get('success') }}
            </div>
        @endif
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Nama Dokter</th>
                  <th>Spesialis</th>
                  <th>No Handphone</th>
                  <th>Alamat</th>
                  <th colspan="2">
                    <center>
                    <a href="{{route('dokter-baru')}}" class="btn btn-sm btn-success mr-2 mb-2">
                        Tambah Data
                    </a>
                    </center>
                   </th>
                </tr>
              </thead>
              <tbody>
                <?php if ($dokter->isEmpty() != TRUE) { ?>
                  <?php $no=1; foreach($dokter as $rowDokter) { ?>
                  <tr> 
                    <td><?php echo $no; ?></td>
                    <td><?php echo $rowDokter->nama_dokter; ?></td>
                    <td><?php echo $rowDokter->spesialis; ?></td>
                    <td><?php echo $rowDokter->no_hp; ?></td>
                    <td><?php echo $rowDokter->alamat; ?></td>
                    <td>
                      <a href="{{route('dokter-detail', ['id' => $rowDokter->id_dokter])}}" class="btn btn-sm btn-primary mr-2 mb-2">
                        Edit
                      </a>
                    </td>
                    <td>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal<?php echo $rowDokter->id_dokter; ?>">
                        Hapus
                      </button>
                    </td>
                  </tr>
                  <?php $no++; } ?>
                <?php } else { ?>
                  <tr>
                    <td colspan="11">
                      Data kosong!
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection