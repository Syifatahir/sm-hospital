@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="bottom">
            <div>
                <table border="0">
                    <tr>
                        <td width="700px">
                        <font color="#000" size="6px"> Layanan </font> <br> <br> <br>
                         </td>
                        <td style="padding-left:20px;"> </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <?php foreach($poli as $rowPoli) { ?>
            <div class="col-3">
            <a href="{{route('poli-detail', ['id' => $rowPoli->id_poli])}}">
                <div class="responsive">
                  <div class="gallery">
                      <img src="<?php echo url('img/'.$rowPoli->gambar_poli); ?>"  width="100%" height="100%">
                    <div class="desc"><?php echo $rowPoli->nama_poli; ?></div>
                  </div>
                </div>   
            </a>
            </div>
            <?php } ?>
        </div>
    </div>
    
@endsection