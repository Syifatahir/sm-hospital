<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="/admin" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item has-treeview">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-folder-open"></i>
        <p>
          Data Pasien
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="<?php echo url(''); ?>/admin/pasien" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Pasien</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview">
      <a href="" class="nav-link">
      <i class="nav-icon fas fa-folder-open"></i>
        <p>
          Poli
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="<?php echo url(''); ?>/admin/dokter" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Dokter</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo url(''); ?>/admin/jadwal" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Jadwal</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
        <a href="<?php echo url(''); ?>/admin/rekam_medis" class="nav-link">
          <i class="nav-icon fas fa-folder-open"></i>
          <p>Rekam Medis</p>
        </a>
      </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-sign-out-alt"></i>
        <p>
          Sign Out
        </p>
      </a>
    </li>
  </ul>
</nav>