@extends('layouts.dashboard')
@section('content')
<!-- <?php  ?> -->
<?php if ($jadwal->isEmpty() != TRUE) { ?>
<?php foreach ($jadwal as $rowjadwal){ ?>
<div class="modal fade" id="exampleModal<?php echo $rowjadwal->id_jadwal; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi penghapusan data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah anda yakin akan menghapus jadwal terpilih?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <a href="{{route('jadwal-destroy', ['id' => $rowjadwal->id_jadwal])}}" class="btn btn-sm btn-danger mr-2 mb-2">
          Hapus
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>
  <!-- table pendaftaran -->
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Data Jadwal</h4>
        </div>
        <div class="card-body">
          <form action="{{URL('/admin/jadwal')}}">
            <div class="row">
              <div class="col">
                <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Cari Jadwal...">
              </div>
              <div class="col-auto">
                <button class="btn btn-primary">
                  Cari
                </button>
              </div>
            </div>
          </form>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable fade show">
              {{ session()->get('success') }}
            </div>
        @endif
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Nama Dokter</th>
                  <th>Nama Poli</th>
                  <th>Hari</th>
                  <th>Jam</th>
                  <th colspan="2">
                    <center>
                        <a href="{{route('jadwal-baru')}}" class="btn btn-sm btn-success mr-2 mb-2">
                            Tambah Data
                        </a>
                    </center>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php if ($jadwal->isEmpty() != TRUE) { ?>
                  <?php $no=1; foreach($jadwal as $rowjadwal) { ?>
                  <tr> 
                    <td><?php echo $no; ?></td>
                    <td><?php echo $rowjadwal->nama_dokter; ?></td>
                    <td><?php echo $rowjadwal->nama_poli; ?></td>
                    <td><?php echo $rowjadwal->hari; ?></td>
                    <td><?php echo $rowjadwal->jam; ?></td>
                    <td>
                      <a href="{{route('jadwal-detail', ['id' => $rowjadwal->id_jadwal])}}" class="btn btn-sm btn-primary mr-2 mb-2">
                        Edit
                      </a>
                    </td>
                    <td>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal<?php echo $rowjadwal->id_jadwal; ?>">
                        Hapus
                      </button>
                    </td>
                  </tr>
                  <?php $no++; } ?>
                <?php } else { ?>
                  <tr>
                    <td colspan="11">
                      Data kosong!
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection