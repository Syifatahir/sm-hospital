@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="bottom">
            <div>
                <table border="0">
                    <tr>
                        <td width="700px">
                        <font color="#000" size="6px"><?php echo $nama; ?></font> <br> <br> <br>
                         </td>
                        <td style="padding-left:20px;"> </td>
                    </tr>
                </table>
            </div>
        </div>
        <a href="{{route('poli')}}" class="btn btn-primary my-3">Kembali</a>
        <h4>Silakan klik salah satu jadwal untuk membuat pendaftaran rekam medis</h4>
        <div class="row">
            <?php foreach($poli as $rowPoli) { ?>
                <div class="col-4">
                    <a href="{{route('rekam_medis-baru', ['id_dokter' => $rowPoli->id_dokter])}}">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Dokter <?php echo $rowPoli->nama_dokter; ?> </h3> <br>
                                <hr>
                                <p class="card-text">Jadwal <br> hari: <?php echo $rowPoli->hari;?> <br> jam: <?php echo $rowPoli->jam; ?>.</p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
    
@endsection