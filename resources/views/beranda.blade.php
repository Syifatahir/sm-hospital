@extends('layouts.app')

@section('content')

	<div class="middle">
		<iframe width="100%" height="500" src="https://www.youtube.com/embed/wQ2TN_gI3sE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>
	
	<div class="bottom">
		<div>
			<table border="0">
				<tr>
					<td width="700px">
						<font color="#000"> SELAMAT DATANG DI SM HOSPITAL </font> <br> <br>

					<font color="#000" size="5px"> We Have Medicare Plan Options for You! </font> <br> <br>

Rumah Sakit SM dirancang bangun tiga lantai, dimana saat ini baru memiliki fasilitas pelayanan Rawat Jalan sejumlah 7 Poliklinik baik umum maupun spesialis, Poliklinik Gigi, HCU, Instalasi Gawat Darurat, Rawat Inap dengan 50 TT, Kamar Bersalin, Kamar Bayi Sehat & Sakit, Kamar Operasi, dan Penunjang Medis berupa Laboratorium, Farmasi, Radiologi, Gizi, Rekam Medik, Mata, Loundry, dan lain lain. <br><br>

Rumah Sakit SM menyediakan 53 tempat tidur perawatan dengan pilihan kamar yang tenang, nyaman, sejuk (semua ruang ber AC), yang terbagi dalam beberapa kelas, antara lain : kamar utama (4 bed), kamar kelas 1 (4 bed), kamar kelas 2 (20 bed), kamar kelas 3 (25 bed), High Care Unit (3 bed). Adapun pelayanan fasilitas pelayanan Medis & Penunjang Medis yang disediakan Rumah Sakit sebagai berikut: HCU (High Care Unit), Klinik Gigi Umum, Klinik Penyakit Dalam, Klinik Anak, Klinik Saraf, Klinik Mata, Klinik Bedah, Kebidanan dan Kandungan (Obsgyn), dan Pijat Fisoterapi. Untuk operasional pelayanan dari lantai 1 sampai dengan lantai 3 masih tangga manual sesuai bangunan, juga disediakan sarana emergensi exit dan Lift untuk barang. Sejak januari 2017 RS SM melayani BPJS , BPJS kesehatan dan Jamkesos. <br><br>

<h3>MENGAPA KAMI?</h3>
<br>
<ul>
<li> <b>Rumah Sakit Berbasis Digital</b> 
	<ol>Menerapkan sistem manajemen operasional berbasis teknologi informasi jaringan</ol>
<li> <b>Perawatan Berkelanjutan</b>
	<ol>Kesinambungan pelayanan yang dilakukan mulai dari perawatan sampai pasien pulang ke rumah oleh tim home care multi-profesi</ol>
<li> <b>Praktik Berbasis Bukti</b>
	<ol>Seluruh klien dikaji berdasarkan riwayat penyakit dahulu, keluarga, dan pola hidup</ol>
<li> <b>Kolaborasi Interprofesional</b>
	<ol>Mengembangkan kolaborasi interprofesional dalam pelayanan, riset bidang kesehatan, serta pengabdian masyarakat</ol>
</ul>
<br>
 </td>
</tr>	
			</table>
		</div>
	</div>

	<div class="nav_down">
		<div>
		 &copy; 2022 SM Hospital
		</div>
	</div>
@endsection
