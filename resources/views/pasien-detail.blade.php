@extends('layouts.app')

@section('content')

<?php ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9"> 
            <div class="card">
                <div class="card-header">{{ __('Update data Pasien') }}</div>

                <div class="card-body">

                    @if(session()->has('success'))
                        <div class="alert alert-success">{{ session()->get('success') }}</div>
                    @endif

                    <form method="POST" action="{{ route('pasien-edit-detail') }}">
                        <input id="no_rm" type="hidden" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="no_rm" value="<?php echo $pasien->no_rm; ?>">
                        @csrf
                        <br>

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="<?php echo $pasien->nama; ?>" autofocus>

                                @if ($errors->has('nama'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="" placeholder="Kosongkan email jika tidak ada perubahan">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        

                        <div class="form-group row">
                            <label for="NIK" class="col-md-4 col-form-label text-md-right">{{ __('NIK') }}</label>

                            <div class="col-md-4">
                                <input id="NIK" type="text" class="form-control{{ $errors->has('NIK') ? ' is-invalid' : '' }}" name="NIK" value="<?php echo $pasien->NIK; ?>" autofocus>

                                @if ($errors->has('NIK'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tempat_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tempat Lahir') }}</label>

                            <div class="col-md-5">
                                <input id="tempat_lahir"  type="text" class="form-control{{ $errors->has('tempat_lahir') ? ' is-invalid' : '' }}" name="tempat_lahir" value="<?php echo $pasien->tempat_lahir; ?>" autofocus>

                                @if ($errors->has('tempat_lahir'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tgl_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

                            <div class="col-md-4">
                                <input id="tgl_lahir" name="tgl_lahir" type="text" class="form-control{{ $errors->has('tgl_lahir') ? ' is-invalid' : '' }}" name="tgl_lahir" value="<?php echo $pasien->tgl_lahir; ?>" autofocus>
                                <small id="passwordHelpBlock" class="form-text text-danger">
                                 Format: YYYY-MM-DD, contoh 1990-11-29.
                                </small>                                

                                @if ($errors->has('tgl_lahir'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="umur" class="col-md-4 col-form-label text-md-right">{{ __('Umur') }}</label>

                            <div class="col-md-4">
                                <input id="umur" name="umur" type="text" class="form-control{{ $errors->has('umur') ? ' is-invalid' : '' }}" name="umur" value="<?php echo $pasien->umur; ?>" autofocus>

                                @if ($errors->has('umur'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jk" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Kelamin') }}</label>

                            <div class="col-md-4">
                                <input id="jk" type="text" class="form-control{{ $errors->has('jk') ? ' is-invalid' : '' }}" name="jk" value="<?php echo $pasien->jk; ?>" autofocus>

                                @if ($errors->has('jk'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Opps!</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


            
                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Lengkap') }}</label>
                            <div class="col-md-6">
                                <textarea id="alamat" type="text" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" name="alamat" value="" rows="3"><?php echo $pasien->alamat; ?></textarea>

                                @if ($errors->has('alamat'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('alamat') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection