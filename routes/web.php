<?php
use App\Http\Controllers\Admin\IndexController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/beranda','berandaController@index')->name('beranda');
Route::get('/pasien', 'pasienController@index')->name('pasien')->middleware('auth');
Route::post('/pasien', 'pasienController@store')->name('daftar-pasien')->middleware('auth');
Route::get('/poli', 'poliController@index')->name('poli')->middleware('auth');
Route::get('/poli/detail/{id}', 'poliController@show')->name('poli-detail')->middleware('auth');


Route::get('/pasienDetail/{id}', 'pasienController@show')->name('pasien-detail');
Route::get('/pasienDestroy/{id}', 'pasienController@destroy')->name('pasien-destroy');
Route::post('/pasienEdit', 'pasienController@edit')->name('pasien-edit-detail');
Route::get('/tentang', 'tentangController@index')->name('tentang');
Route::get('/kontak', 'kontakController@index')->name('kontak');
Route::get('/dokter', 'DokterController@index')->name('dokter-data')->middleware('auth');
Route::get('/dokter/baru', 'DokterController@create')->name('dokter-baru')->middleware('auth');
Route::post('/dokter/simpan', 'DokterController@store')->name('dokter-store')->middleware('auth');
Route::get('/dokterDestroy/{id}', 'dokterController@destroy')->name('dokter-destroy')->middleware('auth');
Route::get('/dokterDetail/{id}', 'dokterController@show')->name('dokter-detail')->middleware('auth');
Route::post('/dokterEdit', 'dokterController@edit')->name('dokter-edit-detail')->middleware('auth');

Route::get('/jadwal', 'jadwalController@index')->name('jadwal-data')->middleware('auth');
Route::get('/jadwal/baru', 'jadwalController@create')->name('jadwal-baru')->middleware('auth');
Route::post('/jadwal/simpan', 'jadwalController@store')->name('jadwal-store')->middleware('auth');
Route::get('/jadwalDestroy/{id}', 'jadwalController@destroy')->name('jadwal-destroy')->middleware('auth');
Route::get('/jadwalDetail/{id}', 'jadwalController@show')->name('jadwal-detail')->middleware('auth');
Route::post('/jadwalEdit', 'jadwalController@edit')->name('jadwal-edit-detail')->middleware('auth');

Route::get('/rekam_medis/baru/{id_dokter}', 'RekamMedisController@create')->name('rekam_medis-baru')->middleware('auth');
Route::get('/rekam_medis/list', 'RekamMedisController@list')->name('rekam_medis-list')->middleware('auth');
Route::get('/rekam_medis/getAll', 'RekamMedisController@getAll')->name('rekam_medis-getAll')->middleware('auth');
Route::post('/rekam_medis/simpan', 'RekamMedisController@store')->name('rekam_medis-store')->middleware('auth');
Route::get('/rekam_medis/detail/{id}', 'RekamMedisController@show')->name('rekam_medis-detail')->middleware('auth');
Route::get('/rekam_medis/destroy/{id}', 'RekamMedisController@destroy')->name('rekam_medis-destroy')->middleware('auth');

// Admin
Route::middleware('auth')->group(function (){
	Route::get('/admin/index', [IndexController::class, 'index'])->name('dashboard');
});

Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'DashboardController@index');
    Route::resource('pasien', 'pasienadminController');
    Route::resource('dokter', 'DokterController');
    Route::resource('jadwal', 'jadwalController');
    Route::resource('dokter/baru', 'DokterController@create');
    Route::resource('poli', 'poliController');
    Route::resource('rekam_medis', 'RekamMedisController');
  });
