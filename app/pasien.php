<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pasien extends Model
{
    
	protected $guarded = [''];
    protected $table = 'pasien';

    public function rekam_medis()
    {
        return $this->hasOne('App\rekam_medis');
    } 
}
