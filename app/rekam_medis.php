<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rekam_medis extends Model
{
	public $timestamps = false;
    protected $table = 'rekam_medis';
	
	protected $fillable = [
        'jenis_pendaftaran',
        'surat_rujukan',
        'tgl_pendaftaran',
        'no_rm',
		'id_dokter'
    ];

    public function pasien()
	{
		return $this->belongsTo('App\pasien');
	} 

	public function dokter()
	{
		return $this->belongsTo('App\dokter');
	}  
}
