<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dokter extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id_dokter';
    protected $table = 'dokter';

    protected $fillable = [
        'nama_dokter',
        'spesialis',
        'no_hp',
        'alamat'
    ];

    public function jadwal()
    {
        return $this->hasMany(jadwal::class);
    }

    public function rekamMedis()
    {
        return $this->hasMany(rekam_medis::class);
    }
}
