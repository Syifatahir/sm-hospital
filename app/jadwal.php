<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jadwal extends Model
{
    protected $table = 'jadwal';
	public $timestamps = false;
	protected $fillable = [
        'id_dokter',
        'id_poli',
        'hari',
        'jam'
    ];
    public function dokter()
	{
		return $this->belongsTo('App\dokter');
	}  

	public function poli()
	{
		return $this->belongsTo('App\poli');
	} 
}
