<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if ($request->admin()->role == $role) {
            return $next($request);
        }
 
        abort(403, 'Anda tidak memiliki hak mengakses laman tersebut!');
    }

    public function handle(Request $request, Closure $next, $role)
    {
        if ($request->admin()->role == $role) {
            return $next($request);
        }
 
        return redirect()
            ->to(route('login'));
    }
}
