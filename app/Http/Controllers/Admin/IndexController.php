<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index() {
        // $pasien = pasien::all();
        // @php dump($pasien); @endphp
    	return view('Admin.index');
    }
}
