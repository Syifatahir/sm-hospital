<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dokter;

class dokterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $cari = $request->keyword;
        $dokter = dokter::where('nama_dokter', 'like', '%'.$cari.'%')-> get();
        
        $data = array('title' => 'Dokter', 'dokter' => $dokter);
        return view ('dokter', $data);
        
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array('title' => 'dokter');
        return view('dokter-baru', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama_dokter' => 'required|max:255',
            'spesialis' => 'required',
            'no_hp' => 'required|max:12',
            'alamat' => 'required'
        ]);

        dokter::create($validateData);
        return redirect()->route('dokter-data')->with('success', "Berhasil Di Upload");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dokter = dokter::where('id_dokter', $id)->first();
        $data = array('title' => 'Dokter', 'dokter' => $dokter);
        return view('dokter-detail', $data)->with('id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id_dokter = $request->id_dokter;
        $nama_dokter = $request->nama_dokter;
        $spesialis = $request->spesialis;
        $no_hp = $request->no_hp;
        $alamat = $request->alamat;
        $validateData = $request->validate([
            'nama_dokter' => 'required|max:255',
            'spesialis' => 'required',
            'no_hp' => 'required|max:12',
            'alamat' => 'required',
        ]);
        dokter::where('id_dokter', $id_dokter)->update([
            'nama_dokter' => $nama_dokter,
            'spesialis' =>$spesialis,
            'no_hp' => $no_hp,
            'alamat' => $alamat,
        ]);
        
        // var_dump($updateDokter);


        // $updateDokter->save();
        return redirect('/admin/dokter')->with('success', "Edit Data Berhasil");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // var_dump($id);
        $dokter = dokter::where('id_dokter', $id)->delete();
        // var_dump($pasien);
        // $pasien->delete();

        return redirect('/admin/dokter')->with('success', "Hapus Data Berhasil");
    }
}