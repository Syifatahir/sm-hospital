<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pasien;

class pasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pasien');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pasien');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, pasien $pasien)
    {        
        $validateData = $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|unique:pasien,email',
            'NIK' => 'required|max:16',
            'tgl_lahir' => 'required',
            'tempat_lahir' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
            'jk' => 'required'
        ]);

        pasien::create($validateData);
        return redirect()->route('pasien')->with('success', "Pendaftaran Berhasil");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pasien = pasien::where('no_rm', $id)->first();
        $data = array('title' => 'Pasien', 'pasien' => $pasien);
        return view('pasien-detail', $data)->with('id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $no_rm = $request->no_rm;
        $nama = $request->nama;
        $email = $request->email;
        $NIK = $request->NIK;
        $tgl_lahir = $request->tgl_lahir;
        $tempat_lahir = $request->tempat_lahir;
        $umur = $request->umur;
        $alamat = $request->alamat;
        $jk = $request->jk;

        if ($email != NULL) {
            $validateData = $request->validate([
                'nama' => 'required|max:255',
                'email' => 'required|unique',
                'NIK' => 'required|max:16',
                'tgl_lahir' => 'required',
                'tempat_lahir' => 'required',
                'umur' => 'required',
                'alamat' => 'required',
                'jk' => 'required'
            ]);
            pasien::where('no_rm', $no_rm)->update([
                'nama' => $nama,
                'email' =>$email,
                'NIK' => $NIK,
                'tgl_lahir' => $tgl_lahir,
                'tempat_lahir' => $tempat_lahir,
                'umur' => $umur,
                'alamat' => $alamat,
                'jk' => $jk,
            ]);
        } else {
            $validateData = $request->validate([
                'nama' => 'required|max:255',
                'NIK' => 'required|max:16',
                'tgl_lahir' => 'required',
                'tempat_lahir' => 'required',
                'umur' => 'required',
                'alamat' => 'required',
                'jk' => 'required'
            ]);
            pasien::where('no_rm', $no_rm)->update([
                'nama' => $nama,
                'NIK' => $NIK,
                'tgl_lahir' => $tgl_lahir,
                'tempat_lahir' => $tempat_lahir,
                'umur' => $umur,
                'alamat' => $alamat,
                'jk' => $jk,
            ]);
        }
        // var_dump($updatePasien);


        // $updatePasien->save();
        return redirect('/admin/pasien')->with('success', "Edit Data Berhasil");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // var_dump($id);
        $pasien = pasien::where('no_rm', $id)->delete();
        // var_dump($pasien);
        // $pasien->delete();

        return redirect('/admin/pasien')->with('success', "Hapus Data Berhasil");
    }
}