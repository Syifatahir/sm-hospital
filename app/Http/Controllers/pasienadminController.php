<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pasien;

class pasienadminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $cari = $request->keyword;
        $pasien = pasien::where('nama', 'like', '%'.$cari.'%')-> get();
        
        $data = array('title' => 'Pasien', 'pasien' => $pasien);
        return view ('pasien.index', $data);
        
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pasien');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, pasien $pasien)
    {        
        $validateData = $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email',
            'NIK' => 'required|max:16',
            'tgl_lahir' => 'required',
            'tempat_lahir' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
            'jk' => 'required'
        ]);

        pasien::create($validateData);
        return redirect()->route('pasien')->with('success', "Pendaftaran Berhasil");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pasien');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
