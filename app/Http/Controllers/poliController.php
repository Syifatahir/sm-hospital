<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\poli;
use Illuminate\Support\Facades\DB;

class poliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poli = poli::all();
        $data = array('title' => 'Poli', 'poli' => $poli);
        return view('poli', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $poli = DB::table('jadwal')
                ->join('poli', 'jadwal.id_poli', '=', 'poli.id_poli')
                ->join('dokter', 'dokter.id_dokter', '=', 'jadwal.id_dokter')
                ->where('jadwal.id_poli', $id)
                ->get();
        $nama = poli::where('id_poli', $id)->pluck('nama_poli')->first();
        $data = array('title' => 'jadwal', 'poli' => $poli, 'nama' => $nama);
        return view('poli-detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
