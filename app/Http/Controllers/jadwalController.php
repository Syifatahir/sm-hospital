<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jadwal;
use App\poli;
use App\dokter;
use Illuminate\Support\Facades\DB;

class jadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->keyword;
        $jadwal = DB::table('jadwal')
                    ->join('poli', 'poli.id_poli', '=', 'jadwal.id_poli')
                    ->join('dokter', 'dokter.id_dokter', '=', 'jadwal.id_dokter')
                    ->where('hari', 'like', '%'.$cari.'%')
                    ->get();
                    
        $data = array('title' => 'jadwal', 'jadwal' => $jadwal);
        return view('jadwal', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $poli = poli::all();
        $dokter = dokter::all();
        $data = array('title' => 'jadwal', 'poli' => $poli, 'dokter' => $dokter);
        return view('jadwal-baru', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, jadwal $jadwal)
    {
        $validateData = $request->validate([
            'hari' => 'required',
            'jam' => 'required',
            'id_dokter' => 'required|max:10',
            'id_poli' => 'required|max:10',
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        jadwal::create($validateData);
        return redirect()->route('jadwal-data')->with('success', "Berhasil Di Upload");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jadwal = DB::table('jadwal')
                ->join('poli', 'poli.id_poli', '=', 'jadwal.id_poli')
                ->join('dokter', 'dokter.id_dokter', '=', 'jadwal.id_dokter')
                ->where('id_jadwal', $id)
                ->first();
        $poli = poli::all();
        $dokter = dokter::all();
        $data = array('title' => 'jadwal', 'jadwal' => $jadwal, 'poli' => $poli, 'dokter' => $dokter);
        return view('jadwal-detail', $data)->with('id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id_jadwal = $request->id_jadwal;
        $hari = $request->hari;
        $jam = $request->jam;
        $id_dokter = $request->id_dokter;
        $id_poli = $request->id_poli;
        // var_dump($id_jadwal);exit();
        $validateData = $request->validate([
            'hari' => 'required',
            'jam' => 'required',
            'id_dokter' => 'required|max:10',
            'id_poli' => 'required|max:10',
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        jadwal::where('id_jadwal', $id_jadwal)->update([
            'hari' => $hari,
            'jam' =>$jam,
            'id_dokter' => $id_dokter,
            'id_poli' => $id_poli,
        ]);
        // var_dump($updatejadwal);


        // $updatejadwal->save();
        return redirect('/admin/jadwal')->with('success', "Edit Data Berhasil");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // var_dump($id);
        $jadwal = jadwal::where('id_jadwal', $id)->delete();
        // var_dump($pasien);
        // $pasien->delete();

        return redirect('/admin/jadwal')->with('success', "Hapus Data Berhasil");
    }
}