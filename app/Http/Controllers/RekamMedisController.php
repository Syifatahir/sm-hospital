<?php

namespace App\Http\Controllers;

use App\dokter;
use App\rekam_medis;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class RekamMedisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->keyword;
        $rekam_medis = DB::table('rekam_medis')
        ->join('dokter', 'dokter.id_dokter', '=', 'rekam_medis.id_dokter')
        ->join('pasien', 'pasien.no_rm', '=', 'rekam_medis.no_rm')
        ->where('rekam_medis.no_rm', 'like', '%'.$cari.'%')
        ->get();
    
        $data = array('title' => 'Daftar Rekam Medis', 'rekam_medis' => $rekam_medis);
        return view('rekam_medis-getAll', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $id_dokter)
    {
        $pasien = Auth::user();
        $dokter = dokter::where('id_dokter', $id_dokter)->first();
        // var_dump($dokter);exit();
        $data = array('title' => 'Pendaftaran', 'dokter' => $dokter, 'pasien' => $pasien);
        return view('rekam_medis-baru', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $no_rm = $request->no_rm;
        $id_dokter = $request->id_dokter;
        $surat_rujukan = $request->surat_rujukan;
        $tgl_pendaftaran = $request->tgl_pendaftaran;
        $jenis_pendaftaran = $request->jenis_pendaftaran;
        // var_dump($jenis_pendaftaran == "BPJS");exit();
        if($jenis_pendaftaran == "BPJS"){
            $validateData = $request->validate([
                'jenis_pendaftaran' => 'required',
                'no_rm' => 'required',
                'id_dokter' => 'required',
                'surat_rujukan' => 'required',
                'tgl_pendaftaran' => 'required',
            ]);
        } else {
            $validateData = $request->validate([
                'jenis_pendaftaran' => 'required',
                'no_rm' => 'required',
                'id_dokter' => 'required',
                'tgl_pendaftaran' => 'required',
            ]);
        }
        if($surat_rujukan == NULL) {
            $surat_rujukan = 0;
        }
        $arr = array(
            'no_rm' => $no_rm,
            'id_dokter' => $id_dokter,
            'surat_rujukan' => $surat_rujukan,
            'tgl_pendaftaran' => $tgl_pendaftaran,
            'jenis_pendaftaran' => $jenis_pendaftaran
        );
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $rekam_medis = rekam_medis::create($arr);
        // var_dump($rekam_medis->id);exit();
        return redirect()->route('rekam_medis-detail', ['id'=>$rekam_medis->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rekam_medis = DB::table('rekam_medis')
        ->join('dokter', 'dokter.id_dokter', '=', 'rekam_medis.id_dokter')
        ->join('users', 'users.id', '=', 'rekam_medis.no_rm')
        ->where('rekam_medis.no_pendaftaran', $id)
        ->first();
        $data = array('title' => 'Detail Pendaftaran', 'rekam_medis' => $rekam_medis);
        return view('rekam_medis-detail', $data)->with('id', $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        // \DB::enableQueryLog();
        $rekam_medis = DB::table('rekam_medis')
        ->join('dokter', 'dokter.id_dokter', '=', 'rekam_medis.id_dokter')
        ->join('users', 'users.id', '=', 'rekam_medis.no_rm')
        ->where('rekam_medis.no_rm', Auth::user()->id)
        ->get();
        // dd(\DB::getQueryLog());
        //var_dump($rekam_medis);exit();
        $data = array('title' => 'Detail Pendaftaran', 'rekam_medis' => $rekam_medis);
        return view('rekam_medis-list', $data);
    }

    public function getAll()
    {
        // \DB::enableQueryLog();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // var_dump($id);
        $rekam_medis = rekam_medis::where('no_pendaftaran', $id)->delete();
        // var_dump($pasien);
        // $pasien->delete();

        return redirect()->action([RekamMedisController::class, 'list'])->with('success', "Hapus Data Berhasil");
    }
}
